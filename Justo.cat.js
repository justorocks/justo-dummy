//imports
const catalog = require("justo").catalog;
const babel = require("justo.plugin.babel");
const cli = require("justo.plugin.cli");
const eslint = require("justo.plugin.eslint");
const fs = require("justo.plugin.fs");
const npm = require("justo.plugin.npm");

//internal data
const PKG = "justo.dummy";

//catalog
catalog.macro("lint", [
  {title: "Check Dogma code", task: cli, params: {cmd: "dogmac lint -l src test"}},
  {title: "Check JavaScript code", task: eslint, params: {src: "."}}
]).title("Lint source code");

catalog.macro("trans-dogma", [
  {title: "Transpile src", task: cli, params: {cmd: "dogmac js -o build/src src"}},
  {title: "Transpile test", task: cli, params: {cmd: "dogmac js -o build/test/unit test/unit"}}
]).title("Transpile from Dogma to JS");

catalog.call("trans-js", babel, {
  src: "build",
  dst: `dist/${PKG}/`
}).title("Transpile from JS to JS");

catalog.macro("build", [
  {task: fs.rm, params: {path: "./build"}},
  {task: fs.rm, params: {path: "./dist"}},
  {task: catalog.get("trans-dogma")},
  {task: catalog.get("trans-js")},
  {task: fs.copy, params: {src: "package.json", dst: `dist/${PKG}/package.json`}},
  {task: fs.copy, params: {src: "README.js.md", dst: `dist/${PKG}/README.md`}},
]).title("Build package");

catalog.macro("make", [
  catalog.get("lint"),
  catalog.get("build")
]).title("Lint and build");

catalog.call("install", npm.install, {
  pkg: `./dist/${PKG}`,
  global: true
}).title("Install package globally");

catalog.call("pub", npm.publish, {
  who: "justojs",
  path: `dist/${PKG}`
}).title("Publish in NPM");

catalog.macro("test", `./dist/${PKG}/test/unit`).title("Unit testing");

catalog.macro("dflt", [
  catalog.get("lint"),
  catalog.get("build"),
  catalog.get("test")
]).title("Lint, build and test");
