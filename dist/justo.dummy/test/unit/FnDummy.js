"use strict";

var _dogmalang = require("dogmalang");

var _justo = require("justo");

const assert = _dogmalang.dogma.use(require("justo.assert"));const dummy = _dogmalang.dogma.use(require("../../../justo.dummy"));
module.exports = exports = (0, _justo.suite)("fn dummy", () => {
  {
    (0, _justo.test)("dummy.func()", () => {
      {
        assert(dummy.func()).isFn();assert(dummy.func()()).isNil();
      }
    });
  }
});