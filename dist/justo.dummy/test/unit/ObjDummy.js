"use strict";

var _dogmalang = require("dogmalang");

var _justo = require("justo");

const assert = _dogmalang.dogma.use(require("justo.assert"));const dummy = _dogmalang.dogma.use(require("../../../justo.dummy"));
const $User = class User {
  constructor(username, password) {
    _dogmalang.dogma.paramExpected("username", username, null);_dogmalang.dogma.paramExpected("password", password, null);Object.defineProperty(this, "username", { value: username, enum: true, writable: true });Object.defineProperty(this, "password", { value: password, enum: true, writable: true });{}
  }
};
const User = new Proxy($User, { apply(receiver, self, args) {
    return new $User(...args);
  } });
User.prototype.changePassword = function (pwd) {
  _dogmalang.dogma.paramExpected("pwd", pwd, null);{
    this.password = pwd;return pwd;
  }
};
module.exports = exports = (0, _justo.suite)("obj dummy", () => {
  {
    (0, _justo.test)("mems as list", () => {
      {
        let o = dummy.obj(User("user", "pwd"), ["changePassword()"]);let d = o._dummy;assert(d._fields).eq([]);assert(d._methods).eq(["changePassword"]);assert(o).isInstanceOf("User").has({ ["username"]: "user", ["password"]: "pwd" });assert(o.username).eq("user");assert(o.changePassword).isFn();assert(o.changePassword("another")).eq(null);assert(o.password).eq("pwd");
      }
    });(0, _justo.suite)("method", () => {
      {
        (0, _justo.test)("dummy(obj, method) - method existing", () => {
          {
            let o = dummy.obj(User("user", "pwd"), "changePassword()");let d = o._dummy;assert(d._fields).eq([]);assert(d._methods).eq(["changePassword"]);assert(o).isInstanceOf("User").has({ ["username"]: "user", ["password"]: "pwd" });assert(o.username).eq("user");assert(o.changePassword).isFn();assert(o.changePassword("another")).eq(null);assert(o.password).eq("pwd");
          }
        });(0, _justo.test)("dummy(obj, method) - method not existing", () => {
          {
            let o = dummy.obj(User("user", "pwd"), "meth()");let d = o._dummy;assert(d._fields).eq([]);assert(d._methods).eq(["meth"]);assert(o).isInstanceOf("User").has({ ["username"]: "user", ["password"]: "pwd" });assert(o.username).eq("user");assert(o.changePassword).isFn();assert(o.meth).isFn();assert(o.changePassword("another")).eq("another");assert(o.password).eq("another");assert(o.meth()).isNil();assert(o.unknown).isNil();assert(() => {
              {
                o.unknown();
              }
            }).raises();
          }
        });(0, _justo.test)("replace method", () => {
          {
            let o = dummy.obj(User("user", "pwd"), "meth()");let d = o._dummy;assert(() => {
              {
                o.meth = 123;
              }
            }).raises("the dummy method 'meth' can't be set.");
          }
        });
      }
    });(0, _justo.suite)("field", () => {
      {
        (0, _justo.suite)("get", () => {
          {
            (0, _justo.test)("dummy(obj, field) - get dummy field", () => {
              {
                let o = dummy.obj(User("user", "pwd"), "@password");let d = o._dummy;assert(d._fields).eq(["password"]);assert(d._methods).eq([]);assert(o.username).eq("user");assert(o.password).eq(null);
              }
            });(0, _justo.test)("dummy(obj, field) - get non-dummy field", () => {
              {
                let o = dummy.obj(User("user", "pwd"), "@password");let d = o._dummy;assert(d._fields).eq(["password"]);assert(d._methods).eq([]);assert(o.username).eq("user");assert(o.password).eq(null);
              }
            });
          }
        });(0, _justo.suite)("set", () => {
          {
            (0, _justo.test)("dummy(obj, field) - set dummy field", () => {
              {
                let o = dummy.obj(User("user", "pwd"), "@pwd");let d = o._dummy;assert(d._fields).eq(["pwd"]);assert(d._methods).eq([]);assert(o).has({ ["username"]: "user", ["password"]: "pwd" });o.pwd = 123;assert(o.pwd).isNil();
              }
            });(0, _justo.test)("set non-dummy field", () => {
              {
                let o = dummy.obj(User("user", "pwd"), "@pwd");let d = o._dummy;o.abc = 123;assert(o.abc).eq(123);
              }
            });
          }
        });
      }
    });(0, _justo.test)("dummy.obj(obj, method) - invalid member syntax", () => {
      {
        assert(() => {
          {
            dummy.obj({}, "method");
          }
        }).raises("invalid member: 'method'. Format: '@field' or 'method()'.");
      }
    });
  }
});