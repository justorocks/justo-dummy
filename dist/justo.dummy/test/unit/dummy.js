"use strict";

var _dogmalang = require("dogmalang");

var _justo = require("justo");

const assert = _dogmalang.dogma.use(require("justo.assert"));const dummy = _dogmalang.dogma.use(require("../../../justo.dummy"));
module.exports = exports = (0, _justo.suite)("justo.dummy", () => {
  {
    (0, _justo.test)("dummy", () => {
      {
        assert(dummy).isMap();assert(dummy.func).isFn();assert(dummy.obj).isFn();
      }
    });
  }
});