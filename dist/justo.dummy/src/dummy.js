"use strict";

var _dogmalang = require("dogmalang");

const FnDummy = _dogmalang.dogma.use(require("./FnDummy"));const ObjProxy = _dogmalang.dogma.use(require("./ObjProxy"));
const dummy = {};module.exports = exports = dummy;
dummy.func = (...args) => {
  {
    return FnDummy();
  }
};
dummy.obj = (...args) => {
  {
    return ObjProxy(...args);
  }
};