"use strict";

var _dogmalang = require("dogmalang");

const $ObjDummy = class ObjDummy {
  constructor(obj, mems) {
    _dogmalang.dogma.paramExpected("obj", obj, null);_dogmalang.dogma.paramExpected("mems", mems, [_dogmalang.list, _dogmalang.text]);mems = (0, _dogmalang.list)(mems);Object.defineProperty(this, "_obj", { value: obj, writable: false });{
      _dogmalang.dogma.update(this, { name: "fields", visib: ":", assign: ":=", value: [] }, { name: "methods", visib: ":", assign: ":=", value: [] });for (let mem of mems) {
        if (_dogmalang.dogma.like(mem, "^@")) {
          _dogmalang.dogma.lshift(this._fields, _dogmalang.dogma.getSlice(mem, 1, -1));
        } else if (_dogmalang.dogma.like(mem, "\\(\\)$")) {
          _dogmalang.dogma.lshift(this._methods, _dogmalang.dogma.getSlice(mem, 0, -3));
        } else {
          _dogmalang.dogma.raise("invalid member: '%s'. Format: '@field' or 'method()'.", mem);
        }
      }
    }
  }
};
const ObjDummy = new Proxy($ObjDummy, { apply(receiver, self, args) {
    return new $ObjDummy(...args);
  } });module.exports = exports = ObjDummy;
ObjDummy.prototype._get = function (name) {
  let resp;_dogmalang.dogma.paramExpected("name", name, _dogmalang.text);{
    if (_dogmalang.dogma.includes(this._fields, name)) {
      resp = null;
    } else if (_dogmalang.dogma.includes(this._methods, name)) {
      resp = () => {
        {}
      };
    } else {
      resp = _dogmalang.dogma.getItem(this._obj, name);
    }
  }return resp;
};
ObjDummy.prototype._set = function (name, val) {
  _dogmalang.dogma.paramExpected("name", name, _dogmalang.text);{
    if (_dogmalang.dogma.includes(this._fields, name)) {
      _dogmalang.dogma.nop();
    } else if (_dogmalang.dogma.includes(this._methods, name)) {
      _dogmalang.dogma.raise("the dummy method '%s' can't be set.", name);
    } else {
      _dogmalang.dogma.setItem("=", this._obj, name, val);
    }
  }
};