"use strict";

var _dogmalang = require("dogmalang");

const ObjDummy = _dogmalang.dogma.use(require("./ObjDummy"));
function ObjProxy(obj, mems) {
  _dogmalang.dogma.paramExpected("obj", obj, null);_dogmalang.dogma.paramExpected("mems", mems, null);{
    Object.defineProperty(obj, "_dummy", { value: ObjDummy(obj, mems) });return (0, _dogmalang.proxy)(obj, { ["get"]: (tgt, prop) => {
        _dogmalang.dogma.paramExpected("tgt", tgt, null);_dogmalang.dogma.paramExpected("prop", prop, null);{
          return tgt._dummy._get(prop);
        }
      }, ["set"]: (tgt, prop, val) => {
        _dogmalang.dogma.paramExpected("tgt", tgt, null);_dogmalang.dogma.paramExpected("prop", prop, null);{
          tgt._dummy._set(prop, val);return true;
        }
      } });
  }
}module.exports = exports = ObjProxy;