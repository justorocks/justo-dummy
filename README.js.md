# justo.dummy

A test dummy library.

*Proudly made with ♥ in Valencia, Spain, EU.*

*Developed in [Dogma](http://dogmalang.com), compiled to JavaScript.*

## Module

The library must be included using the module `justo.dummy` and importing as follows:

```
import dummy from "justo.dummy";
```

`dummy` is an object to create dummies.

## Test doubles

A **test double** is an object representing other, used for performing unit tests.
Types:

- **Test dummies**. Test double responding to calls with no action.
- **Test stubs**. Test double responding to calls with predefined responses.
- **Test spies**. Test double monitoring the calls to a function or to an object.

## Test dummies

A **test dummy** is an object that represents other: a function or an object.
A **function dummy** does nothing when called.
An **object dummy** does nothing when its doubled members are called.

### Function dummy

A **function dummy** does nothing.
These are created using `dummy.func()`:

```
function dummy.func() : function
```

`dummy.func()` returns a function dummy.

Example:

```
var fn = dummy.func()
fn()              //does nothing
fn(12, 34, 56)    //does nothing
```

### Object dummy

An **object dummy** is an object whose doubled members does nothing.
These are created using `dummy.obj()`:

```
function dummy.obj(object, members) : object
```

The parameter `object` is the object to double.
The parameter `members` indicates the member names to dummy:

- For an attribute or field, the syntax is `@name`.
- For a method, the format is `method()`.

Example:

```
var user = dummy.obj({username: "user01", password: "pwd"}, "changePassword()")
user.changePassword() --does nothing
user.changePwd()      --raises error due to the method doesn't exist

var user = dummy({username: "user01", password: "pwd"}, ["changePassword()", "otherMethod()"])
user.changePassword() //does nothing
user.otherMethod()    //does nothing
user.changePwd()      //raises error due to the method doesn't exist
```
